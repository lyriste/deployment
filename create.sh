#!/bin/bash

cat <<EOT > roles/services/templates/docker-compose.$1.yml.j2
version: "3"

networks:
  traefik_net:
    external: true

services:
  $1:
    image: 
    container_name: $1
    restart: unless-stopped
    networks:
      - traefik_net
    volumes:
      - "{{ volumes.services }}/$1/config:/config"
    environment:
      TZ: "{{ tz }}"
      PUID: "{{ uid }}"
      PGID: "{{ gid }}"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.$1.rule=Host(\`$1.{{ domain }}\`)"
      - "traefik.http.routers.$1.entrypoints=websecure"
      - "traefik.http.routers.$1.middlewares=authelia@docker"
EOT

cat <<EOT >> roles/services/defaults/main.yml
    - name: $1
EOT