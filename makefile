encrypt: 
	ansible-vault encrypt --vault-password-file .vault_pass vars/vault.yml

decrypt: 
	ansible-vault decrypt --vault-password-file .vault_pass vars/vault.yml

install:
	ansible-galaxy install -r requirements.yml
	ansible-galaxy collection install -r requirements.yml

check-setup:
	ansible-playbook --vault-password-file .vault_pass -C -D setup.yml

check-deploy:
	ansible-playbook --vault-password-file .vault_pass -C -D deploy.yml

setup:
	ansible-playbook --vault-password-file .vault_pass setup.yml

deploy:
	ansible-playbook --vault-password-file .vault_pass deploy.yml

create:
	./create.sh $(service)