#!/bin/bash

docker run --rm -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --monitor-only --run-once
zfs-auto-snapshot --label=update --keep=2 //
systemctl restart lyriste
docker system prune
cd /tank/lyriste/services/authentik
docker-compose run --rm server migrate